# Create an AWS Security Group
module "windows_sg" {
  #source = "git::https://bitbucket.org/deanbantleman/aws-terraform-modules.git//SecurityGroup?ref=v0.0.1"
  source = "C:/Users/dean.bantleman/Google Drive/Scripts/Terraform-AWS/Modules/SecurityGroup"
  aws_access_key = "${var.aws_access_key}"
  aws_secret_key = "${var.aws_secret_key}"
  Win_SG_Name = "${var.Win_SG_Name}"
  region = "${var.region}"
  availability_zone = "${var.availability_zone}"
}

resource "aws_security_group_rule" "SSH" {
  type = "ingress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${module.windows_sg.winsec_id}"
}
