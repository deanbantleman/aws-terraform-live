
#Configure the AWS Provider
variable "region" {
  description = "Enter the region you want to build your resource in, e.g 'us-east-1','eu-west-2'"
}

variable "availability_zone" {
  description = "Select your availability zone, '1','2','3'"
}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.region}" #"us-east-1"
}

variable "Linux_ami_type" {
  type = "map"
  default = {
    us-east-1 = "ami-40d28157"
    eu-west-2 = "ami-0274e11dced17bb5b"
  }
}

variable "Windows_ami_type" {
  type = "map"
  default = {
    us-east-1 = "ami-041114ddee4a98333"
    eu-west-2 = "ami-06a27ce600d784c71"
  }
}

variable "instance_type" {
  type = "map"
  default = {
    dev = "t2.micro"
    prod = "t2.medium"
  } 
}

variable "aws_access_key" {
  description = "Passes AWS access key ID"
}

variable "aws_secret_key" {
  description = "Passes AWS secret access key"
}

variable "INSTANCE_USERNAME" { default = "admin" } 


# Network variables
variable "Win_SG_Name" {
  default = "Windows_SG"
}

variable "Windows_sg_port_3389" {
  default = 3389
}

variable "Windows_sg_port_winrm" {
  type = "list"
  default = ["5985","5986"]
}

variable "Sg_port_HTTP" {
  default = 80
}

variable "Sg_port_HTTPS" {
  default = 443
}

variable "Sg_port_DNS" {
  default = 53
}
